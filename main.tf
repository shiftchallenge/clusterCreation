
locals {
    # ... other locals ...
    clusterNames  = ["cluster1", "cluster2", "cluster3"]
    clusterParams = { 
        cluster1 = { 
          env = "dev"
          diskSize = "25"
        }
        cluster2 = { 
          env = "staging"
          diskSize = "30"
        }
        cluster3 = { 
          env = "prod"
          diskSize = "50"
        }
    }
}

module "clusterCreation" {
    count = length(local.clusterNames)
    source = "./modules/clusterCreation/"
    
    envName     = lookup(lookup(local.clusterParams, local.clusterNames[count.index] ), "env")
    clusterName = local.clusterNames[count.index]
    diskSize    = lookup(lookup(local.clusterParams, local.clusterNames[count.index] ), "diskSize")
}
