terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.28.0"
    }
  }
}
provider "google" {
  credentials = file("./profound-matter-195111-51b92197bb5b.json")

  # project = "<PROJECT_ID>"
  # region  = "us-central1"
  # zone    = "us-central1-c"
}
